# -*- coding: utf-8 -*-
"""
Created on Tue Jan 05 10:06:48 2016

@author: AOsipenko
"""

import vkontakte 
import urllib
from time import sleep
from time import time



token = 'your vk token'

vk = vkontakte.API(token = token)

        
def wall_get_image (name_of_the_group,i, path_to_local_folder):

    for i in range(1,i):
        
        try:
            wall_get =  vk.wall.get(domain = name_of_the_group, count = 1, offset = i )
            url = wall_get[1]['attachments'][1]['doc']['url']
            urllib.urlretrieve(url, path_to_local_folder + '\Picture '+ str(i)+ '.png')
            sleep (0.1)

        except: 
            pass



print "Pictures saving started!"       
                          
t0 = time()

path = '...\local_folder'
name_of_the_group = 'Name of VK group needs to be string'
q = 'Quantity of pictures to save needs to be integer'

wall_get_image(name_of_the_group, q, path)

print "Picture saving time:", round((round((time()-t0), 3)/60),1), "min"
